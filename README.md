## timeline.kde.org
Source code for timeline.kde.org

Translation is carried out by l10n teams with PO files as usual. The file _js/languages.xml_ is handled by scripts and should not be edited manually.